package com.jts.complaintscatalog.addticket;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jts.complaintscatalog.R;

public class AddTicketActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_ticket);
    }
}
