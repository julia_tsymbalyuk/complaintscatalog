package com.jts.complaintscatalog.profile;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.jts.complaintscatalog.BR;
import com.jts.complaintscatalog.R;
import com.jts.complaintscatalog.data.user.User;
import com.jts.complaintscatalog.databinding.ActivityProfileBinding;
import com.jts.complaintscatalog.utils.manager.RealmController;
import com.squareup.picasso.Picasso;

//Shows user profile with Facebook info about user
public class ProfileActivity extends AppCompatActivity{
    private static final String ARG_USER_ID = "userId";
    private ActivityProfileBinding mBinding;
    private RealmController mRealmController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindUser();
    }

    private void bindUser(){
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_profile);

        String userId = getIntent().getStringExtra(ARG_USER_ID);

        mRealmController = RealmController.with(this);

        User user = mRealmController.getUser(userId);

        mBinding.setVariable(BR.user, user);
        mBinding.executePendingBindings();

        ImageView mImageView = (ImageView) findViewById(R.id.user_picture);
        Picasso.with(this).load(user.getImage()).into(mImageView);
    }

}