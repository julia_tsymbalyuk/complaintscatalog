/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the My Own Virtual License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.fakelicenses.com/license
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jts.complaintscatalog.tickets;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jts.complaintscatalog.BR;
import com.jts.complaintscatalog.R;
import com.jts.complaintscatalog.addticket.AddTicketActivity;
import com.jts.complaintscatalog.data.ticket.Ticket;
import com.jts.complaintscatalog.data.ticket.TicketTab;
import com.jts.complaintscatalog.ticketdetail.TicketDetailActivity;
import com.jts.complaintscatalog.utils.manager.ApplicationHelper;
import com.jts.complaintscatalog.utils.manager.NetworkService;
import com.jts.complaintscatalog.utils.manager.RealmController;
import com.jts.complaintscatalog.utils.pagination.PaginationTool;
import com.jts.complaintscatalog.utils.pagination.PagingListener;

import java.util.List;

import io.realm.OrderedRealmCollection;
import io.realm.RealmChangeListener;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;
import rx.Observable;

/**
 * A fragment representing a list of Tickets
 * <p/>
 */
public class TicketsFragment extends Fragment implements TicketsContract.View {

    private static final String ARG_TICKETS_LIST_STATUS = "status";
    private static final int REQUEST_ADD_TICKET = 1;
    private static final String ARG_TICKET_ID = "ticket_id";
    private static final int LIMIT = 5;
    private static final int ZERO_OFFSET = 0;
    private TicketTab mTicketStatus;
    private TicketsContract.UserActionsListener mPresenter;
    private RecyclerView mTicketsRecyclerView;
    private TicketsAdapter mTicketsAdapter;
    private NetworkService mService;
    private RealmController mRealmController;
    private RealmResults<Ticket> mTickets;

    //Realm change listener that refreshes the UI when there is changes to Realm.
    private RealmChangeListener mRealmResultsListener = new RealmChangeListener<RealmResults<Ticket>>() {
        @Override
        public void onChange(RealmResults<Ticket> realmResults) {
            mTicketsAdapter.notifyDataSetChanged();
        }
    };

    public TicketsFragment() {
    }

    //Creates  new instance of this fragment and put Ticket status as argument
    public static TicketsFragment newInstance(TicketTab status) {
        TicketsFragment fragment = new TicketsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_TICKETS_LIST_STATUS, status);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mService = ApplicationHelper.getNetworkService();
        mPresenter = new TicketsPresenter(this, mService);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tickets_overview_list, container, false);
        init(view, savedInstanceState);
        return view;
    }

    private void init(View view, Bundle savedInstanceState) {
        Context context = view.getContext();
        mTicketStatus = (TicketTab) getArguments().getSerializable(ARG_TICKETS_LIST_STATUS);

        //Set listener to floating action button
        FloatingActionButton floatingActionButton = (FloatingActionButton) view.findViewById(R.id.floating_action_button);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.addNewTicket();
            }
        });

        mRealmController = RealmController.with(this);
        mTickets = mRealmController.getTickets(mTicketStatus);

        // init adapter for the first time
        mTicketsAdapter = new TicketsAdapter(context, mTickets);

        //Manage recycler view
        mTicketsRecyclerView = (RecyclerView) view.findViewById(R.id.list);
        mTicketsRecyclerView.setSaveEnabled(true);
        mTicketsRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mTicketsRecyclerView.setAdapter(mTicketsAdapter);

        // RecyclerView pagination
        PaginationTool<List<Ticket>> paginationTool = PaginationTool.buildPagingObservable(mTicketsRecyclerView,
                new PagingListener<List<Ticket>>() {
                    @Override
                    public Observable<List<Ticket>> onNextPage(int offset) {
                        return mPresenter.loadTickets(mTicketStatus, offset);
                    }
                })
                .setLimit(LIMIT)
                .build();

        // Pull-to-refresh
        SwipeRefreshLayout swipeRefreshLayout =
                (SwipeRefreshLayout) view.findViewById(R.id.refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.primary),
                ContextCompat.getColor(getActivity(), R.color.accent),
                ContextCompat.getColor(getActivity(), R.color.primaryDark));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                mRealmController.deleteTickets(mTicketStatus);
                mPresenter.loadTickets(mTicketStatus, ZERO_OFFSET);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // If a note was successfully added, show snackbar
        if (REQUEST_ADD_TICKET == requestCode && Activity.RESULT_OK == resultCode) {
            Snackbar.make(getView(), getString(R.string.successfully_saved_ticket_message),
                    Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if ((mRealmController.getTickets(mTicketStatus)).isEmpty()) {
            mPresenter.loadTickets(mTicketStatus, ZERO_OFFSET);
        }

        // Enable UI refresh while the fragment is active.
        mTickets.addChangeListener(mRealmResultsListener);
    }

    @Override
    public void setProgressIndicator(final boolean active) {
        if (getView() == null) {
            return;
        }
        final SwipeRefreshLayout srl =
                (SwipeRefreshLayout) getView().findViewById(R.id.refresh_layout);

        srl.post(new Runnable() {
            @Override
            public void run() {
                srl.setRefreshing(active);
            }
        });
    }

    @Override
    public void showTickets(List<Ticket> tickets) {
        mTickets = mRealmController.saveTickets(tickets);
    }

    @Override
    public void showTicketsLoadingFailure(Throwable throwable) {
        Log.d("DEBUG", "Tickets loading failed. Cause:" + throwable.getCause().getMessage());
        Toast.makeText(this.getContext(), R.string.error_loading, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showAddTicket() {
        //TODO: make AddTicketActivity and layout valid
        Intent intent = new Intent(getContext(), AddTicketActivity.class);
        startActivityForResult(intent, REQUEST_ADD_TICKET);
    }

    @Override
    public void showTicketDetailUi(Ticket ticket) {
        Intent intent = new Intent(getContext(), TicketDetailActivity.class);
        intent.putExtra(ARG_TICKET_ID, ticket.getId());
        startActivity(intent);
    }

    @Override
    public void onPause() {
        super.onPause();
        mTickets.removeChangeListeners();
    }

    @Override
    public void onDestroyView() {
        // for memory leak prevention (RecycleView is not unsubscibed from adapter DataObserver)
        if (mTicketsRecyclerView != null) {
            mTicketsRecyclerView.setAdapter(null);
        }

        mPresenter.rxUnSubscribe();
        super.onDestroyView();
    }

    /**
     * {@link RealmRecyclerViewAdapter} that can display a overviews of {@link Ticket} Realm objects in RecyclerView
     */
    private class TicketsAdapter extends RealmRecyclerViewAdapter<Ticket, TicketsAdapter.ViewHolder> {

        public TicketsAdapter(Context context, OrderedRealmCollection data) {
            super(context, data, true);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.ticket_overview_item, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final Ticket ticket = getData().get(position);
            holder.getBinding().setVariable(BR.ticket, ticket);
            holder.getBinding().executePendingBindings();

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPresenter.openTicketDetails(ticket);
                }
            });
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private View mView;
            private ViewDataBinding mBinding;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mBinding = DataBindingUtil.bind(view);
            }

            public ViewDataBinding getBinding() {
                return mBinding;
            }
        }
    }

}
