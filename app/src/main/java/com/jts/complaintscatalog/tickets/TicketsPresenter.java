package com.jts.complaintscatalog.tickets;


import com.jts.complaintscatalog.data.ticket.Ticket;
import com.jts.complaintscatalog.data.ticket.TicketTab;
import com.jts.complaintscatalog.utils.manager.NetworkService;

import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.Subscription;

/**
 * Listens to user actions from the UI ({@link TicketsFragment}), retrieves the data and updates the
 * UI as required.
 */
public class TicketsPresenter implements TicketsContract.UserActionsListener {

    private TicketsContract.View mTicketsView;
    private NetworkService mService;
    private Subscription mSubscription;

    public TicketsPresenter(TicketsContract.View mTicketsView, NetworkService mService) {
        this.mTicketsView = mTicketsView;
        this.mService = mService;
    }

    @Override
    public Observable<List<Ticket>> loadTickets(final TicketTab ticketStatus, final int offset) {
        mTicketsView.setProgressIndicator(true);

        Observable<List<Ticket>> ticketsResponseObservable = (Observable<List<Ticket>>)
                mService.getPreparedObservable(mService.getAPI().getTicketsObservable(ticketStatus.getTicketStates(), offset),
                        ticketStatus, true, true);

        mSubscription = ticketsResponseObservable.subscribe(new Observer<List<Ticket>>() {

            @Override
            public void onNext(List<Ticket> response) {
                mTicketsView.setProgressIndicator(false);
                mTicketsView.showTickets(response);
            }

            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                mTicketsView.setProgressIndicator(false);
                mTicketsView.showTicketsLoadingFailure(e);
            }
        });

        return ticketsResponseObservable;
    }

    @Override
    public void addNewTicket() {
        mTicketsView.showAddTicket();
    }

    @Override
    public void openTicketDetails(Ticket requestedTicket) {
        mTicketsView.showTicketDetailUi(requestedTicket);
    }

    @Override
    public void rxUnSubscribe() {
        if (mSubscription != null && !mSubscription.isUnsubscribed())
            mSubscription.unsubscribe();
    }
}
