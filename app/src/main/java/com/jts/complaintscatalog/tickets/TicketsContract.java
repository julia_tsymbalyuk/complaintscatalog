package com.jts.complaintscatalog.tickets;

import android.support.annotation.NonNull;

import com.jts.complaintscatalog.data.ticket.Ticket;
import com.jts.complaintscatalog.data.ticket.TicketTab;

import java.util.List;

import rx.Observable;

/**
 * This specifies the contract between the view and the presenter.
 */
public interface TicketsContract {

    interface View {

        void setProgressIndicator(boolean active);

        void showTickets(List<Ticket> tickets);

        void showTicketsLoadingFailure(Throwable throwable);

        void showAddTicket();

        void showTicketDetailUi(Ticket ticket);
    }

    interface UserActionsListener {

        Observable<List<Ticket>> loadTickets(TicketTab ticketStatus, int offset);

        void addNewTicket();

        void openTicketDetails(@NonNull Ticket requestedTicket);

        void rxUnSubscribe();
    }
}
