package com.jts.complaintscatalog.ticketdetail;

import com.jts.complaintscatalog.data.ticket.TicketAttach;

import java.util.List;

/**
 * This specifies the contract between the view and the presenter.
 */
public interface TicketDetailContract {
    interface View {
        void setStatusBarName(String name);
        void setTicketStatusBackground(int ticketStateId);
        void setImagesToAdapter(List<TicketAttach> ticketAttaches);
    }

    interface UserActionsListener{
        void openTicket(String ticketId);
    }
}
