/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the My Own Virtual License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.fakelicenses.com/license
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jts.complaintscatalog.ticketdetail;

import android.content.Context;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.jts.complaintscatalog.R;
import com.jts.complaintscatalog.data.ticket.TicketAttach;
import com.jts.complaintscatalog.databinding.TicketLayoutBinding;
import com.jts.complaintscatalog.utils.manager.NetworkService;
import com.squareup.picasso.Picasso;

import java.util.List;

/*
* Shows ticket details (description, status, dates, images)
* Generates toast notification after click on every control
*/
public class TicketDetailActivity extends AppCompatActivity implements TicketDetailContract.View {
    private static final String ARG_TICKET_ID = "ticket_id";
    private RecyclerView mRecycleViewImages;
    private Resources mResources;
    private TicketDetailPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mResources = getResources();

        TicketLayoutBinding binding = DataBindingUtil.setContentView(this, R.layout.ticket_layout);
        mPresenter = new TicketDetailPresenter(this, binding);

        setUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        String ticketId = getIntent().getStringExtra(ARG_TICKET_ID);
        mPresenter.openTicket(ticketId);
    }

    //Creates app bar, sets names to UI elements
    private void setUI() {

        //Set toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        //Set status bar color for android versions > 5
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window w = getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setStatusBarColor(ContextCompat.getColor(this, R.color.primaryDark));
        }

        //Set focus on scroll view container itself, preventing its bottom elements take focus
        ScrollView scrollView = (ScrollView) findViewById(R.id.ticket_scrollView);
        scrollView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);

        //Initialize recycler view
        mRecycleViewImages = (RecyclerView) findViewById(R.id.ticket_rv_photos);
        mRecycleViewImages.setHasFixedSize(true);
        RecyclerView.LayoutManager mPhotosLayoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRecycleViewImages.setLayoutManager(mPhotosLayoutManager);
        RecyclerView.ItemAnimator animator = new DefaultItemAnimator();
        mRecycleViewImages.setItemAnimator(animator);
    }

    public void setStatusBarName(String name) {
        getSupportActionBar().setTitle(name);
    }

    public void setTicketStatusBackground(int ticketStateId) {
        //Set background to all text views according to their status
        TextView textViewUiTicketStatus = (TextView) findViewById(R.id.ticket_ui_status);

        switch (ticketStateId) {
            case 0:
            case 9:
            case 5:
            case 7:
            case 8:
                textViewUiTicketStatus.setBackgroundResource(R.drawable.ticket_status_background_in_progress);
                break;
            case 10:
            case 6:
                textViewUiTicketStatus.setBackgroundResource(R.drawable.ticket_status_background_done);
                break;
            case 1:
            case 3:
            case 4:
                textViewUiTicketStatus.setBackgroundResource(R.drawable.ticket_status_background_pending);
        }
    }

    //Fill recycle view with images
    public void setImagesToAdapter(List<TicketAttach> ticketAttaches){
        RecyclerView.Adapter photosAdapter = new ImagesAdapter(ticketAttaches);
        mRecycleViewImages.setAdapter(photosAdapter);
    }

    //Finishes activity after Back button in the app bar is pressed
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return false;
    }


    /* Shows the toast with the name of a view that was clicked.
    *  Format ["View class: "][class package].[class name]
    *         ["View name: "][package]:["id"]/[view id]
    * */

    public void ShowViewNameOnClick(View v) {

        if (mResources == null) {
            mResources = getResources();
        }

        Toast msg =
                Toast.makeText(this, "View class: " + v.getClass()
                        + "\nView name: " + mResources.getResourceName(v.getId()), Toast.LENGTH_LONG);
        msg.show();
    }

    /**
     * Binds images with recycler view. Implements ViewHolder pattern
     */
    public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ViewHolder> {
        private Context mContext;
        private List<TicketAttach> mTicketAttaches;
        private int mImageHeight;

        public ImagesAdapter(List<TicketAttach> ticketAttaches) {
            mTicketAttaches = ticketAttaches;
        }

        //Creates view holder with image view that could be reused in future
        @Override
        public ImagesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            mContext = parent.getContext();
            mImageHeight = Math.round(mContext.getResources().getDimension(R.dimen.recycle_view_height));
            View image = LayoutInflater.from(mContext).inflate(R.layout.img_recycler_view, parent, false);
            return new ViewHolder(image);
        }

        @Override
        public int getItemCount() {
            return mTicketAttaches.size();
        }

        //Fills image view with image from array with specified position. Using Picasso library
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            TicketAttach ticketAttach = mTicketAttaches.get(position);
            String attachUrl = NetworkService.GET_TICKET_ATTACH_URL
                    .concat(ticketAttach.getFilename());

            Picasso.with(mContext)
                    .load(attachUrl)
                    .resize(mImageHeight, mImageHeight)
                    .centerCrop()
                    .into(holder.imgView);
        }

        //Stores links to reusable image views
        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView imgView;

            public ViewHolder(View itemView) {
                super(itemView);
                imgView = (ImageView) itemView;
            }
        }
    }
}
