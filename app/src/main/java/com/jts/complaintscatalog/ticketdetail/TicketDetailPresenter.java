package com.jts.complaintscatalog.ticketdetail;

import android.databinding.ViewDataBinding;

import com.jts.complaintscatalog.BR;
import com.jts.complaintscatalog.data.ticket.Performer;
import com.jts.complaintscatalog.data.ticket.Ticket;
import com.jts.complaintscatalog.utils.manager.ApplicationHelper;

public class TicketDetailPresenter implements TicketDetailContract.UserActionsListener {
    private TicketDetailContract.View mView;
    private ViewDataBinding mBinding;

    public TicketDetailPresenter(TicketDetailContract.View view, ViewDataBinding binding) {
        mView = view;
        mBinding = binding;
    }

    @Override
    public void openTicket(String ticketId) {
        Ticket ticket = ApplicationHelper.getRealmController().getTicket(ticketId);
        Performer performer = null;
        if (!(ticket.getPerformers()).isEmpty()) {
            performer = (Performer) ticket.getPerformers().get(0);
        }

        mBinding.setVariable(BR.ticket, ticket);
        mBinding.setVariable(BR.performer, performer);
        mBinding.executePendingBindings();

        mView.setStatusBarName(ticket.getTicketCategory().getName());
        mView.setTicketStatusBackground(ticket.getTicketState().getId());
        mView.setImagesToAdapter(ticket.getFiles());
    }
}
