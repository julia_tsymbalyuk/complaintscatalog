package com.jts.complaintscatalog.utils.manager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.jts.complaintscatalog.R;
import com.jts.complaintscatalog.data.user.User;
import com.jts.complaintscatalog.profile.ProfileActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

//Manages work with Facebook SDK
public class FacebookManager {
    private static final String ARG_USER_ID = "userId";
    private static final String FACEBOOK_ARG_ID = "id";
    private static final String FACEBOOK_ARG_FIRST_NAME = "first_name";
    private static final String FACEBOOK_ARG_EMAIL = "email";
    private static final String FACEBOOK_ARG_BIRTHDAY = "birthday";
    private static final String FACEBOOK_ARG_PICTURE = "picture";
    private static final String FACEBOOK_ARG_DATA = "data";
    private static final String FACEBOOK_ARG_URL = "url";
    private static final String FACEBOOK_ARG_FIELDS = "fields";
    private static final String FACEBOOK_ARGS_PERMISSIONS = "id,first_name,last_name,email,birthday,picture.type(large)";
    private static final String FACEBOOK_READ_PERMISSION_PROFILE = "public_profile";
    private static final String FACEBOOK_READ_PERMISSION_EMAIL = "email";
    private static final String FACEBOOK_READ_PERMISSION_BIRTHDAY = "user_birthday";
    private Activity mActivity;
    private CallbackManager mCallbackManager;
    private RealmController mRealmController;


    public FacebookManager(Activity activity) {
        mActivity = activity;
        init();
    }

    private void init() {
        FacebookSdk.sdkInitialize(mActivity.getApplicationContext());
        mCallbackManager = CallbackManager.Factory.create();
        mRealmController = RealmController.with(mActivity);

        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d("DEBUG", "Successful Login");
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        try {
                                            User user = new User();
                                            user.setId(object.getString(FACEBOOK_ARG_ID));
                                            user.setFirstName(object.getString(FACEBOOK_ARG_FIRST_NAME));
                                            user.setEmail(object.getString(FACEBOOK_ARG_EMAIL));
                                            if (object.has(FACEBOOK_ARG_BIRTHDAY)) {
                                                user.setBirthday(object.getString(FACEBOOK_ARG_BIRTHDAY));
                                            }
                                            if (object.has(FACEBOOK_ARG_PICTURE)) {
                                                user.setImage(object.getJSONObject(FACEBOOK_ARG_PICTURE)
                                                        .getJSONObject(FACEBOOK_ARG_DATA).getString(FACEBOOK_ARG_URL));
                                            }
                                            AccessToken accessToken = AccessToken.getCurrentAccessToken();
                                            user.setFacebookAccessToken(accessToken.getToken());

                                            mRealmController.saveUser(user);

                                            Intent intent = new Intent(mActivity, ProfileActivity.class);
                                            intent.putExtra(ARG_USER_ID, user.getId());
                                            mActivity.startActivity(intent);

                                        } catch (JSONException e) {
                                            Log.d("DEBUG", "Get incorrect Facebook results");
                                            e.printStackTrace();
                                        }
                                    }
                                });

                        Bundle parameters = new Bundle();
                        parameters.putString(FACEBOOK_ARG_FIELDS, FACEBOOK_ARGS_PERMISSIONS);
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(mActivity, R.string.facebook_login_canceled, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(mActivity, exception.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    public void makeLogin() {
        User user = null;
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null) {
            user = mRealmController.getUserIfLoggedIn(accessToken);
            Log.d("DEBUG", "User is already logged in");
        }
        if (user != null) {
            Intent intent = new Intent(mActivity, ProfileActivity.class);
            intent.putExtra(ARG_USER_ID, user.getId());
            mActivity.startActivity(intent);
        } else {
            Log.d("DEBUG", "User is null, so user wasn't loged in before");
            LoginManager.getInstance().logInWithReadPermissions(mActivity,
                    Arrays.asList(FACEBOOK_READ_PERMISSION_PROFILE,
                            FACEBOOK_READ_PERMISSION_EMAIL,
                            FACEBOOK_READ_PERMISSION_BIRTHDAY));
        }
    }

    public void notifyActivityLoginResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

}
