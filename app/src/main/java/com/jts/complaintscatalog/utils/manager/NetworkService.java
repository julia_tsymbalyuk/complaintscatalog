package com.jts.complaintscatalog.utils.manager;

import android.support.v4.util.LruCache;

import com.jts.complaintscatalog.data.ticket.Ticket;
import com.jts.complaintscatalog.data.ticket.TicketTab;

import java.util.List;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class NetworkService {

    public static final String BASE_URL = "http://dev-contact.yalantis.com";
    public static final String GET_TICKET_ATTACH_URL = "http://dev-contact.yalantis.com/files/ticket/";
    private NetworkAPI networkAPI;
    private LruCache<TicketTab, Observable<?>> apiObservables;

    public NetworkService() {
        this(BASE_URL);
    }

    public NetworkService(String baseUrl) {
        apiObservables = new LruCache<>(10);

        //Retrofit logging. Uncomment to enable
//        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
////        builder.readTimeout(10, TimeUnit.SECONDS);
////        builder.connectTimeout(5, TimeUnit.SECONDS);
//
//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        builder.addInterceptor(interceptor);
//
//        OkHttpClient client = builder.build();

        //Uncomment .client(client) to enable logging
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
//                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        networkAPI = retrofit.create(NetworkAPI.class);
    }

    /**
     * Method to return the API interface.
     *
     * @return
     */
    public NetworkAPI getAPI() {
        return networkAPI;
    }

    /**
     * Method to clear the entire cache of observables
     */
    public void clearCache() {
        apiObservables.evictAll();
    }


    /**
     * Method to either return a cached observable or prepare a new one.
     *
     * @param unPreparedObservable
     * @param status
     * @param cacheObservable
     * @param useCache
     * @return Observable ready to be subscribed to
     */
    public Observable<?> getPreparedObservable(Observable<?> unPreparedObservable, TicketTab status, boolean cacheObservable, boolean useCache) {

        Observable<?> preparedObservable = null;

        if (useCache) {
            //this way we don't reset anything in the cache if this is the only instance of us not wanting to use it.
            preparedObservable = apiObservables.get(status);
        }

        if (preparedObservable != null) {
            return preparedObservable;
        }

        //we are here because we have never created this observable before or we didn't want to use the cache...

        preparedObservable = unPreparedObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        if (cacheObservable) {
            preparedObservable = preparedObservable.cache();
            apiObservables.put(status, preparedObservable);
        }

        return preparedObservable;
    }


    /**
     * all the Service alls to use for the retrofit requests.
     */
    public interface NetworkAPI {
        @GET("/rest/v1/tickets?")
        Observable<List<Ticket>> getTicketsObservable(@Query("state") String ticketStates, @Query("offset") int offset);
    }

}
