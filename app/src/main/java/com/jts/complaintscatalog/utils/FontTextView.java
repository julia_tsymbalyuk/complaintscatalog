/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the My Own Virtual License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.fakelicenses.com/license
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jts.complaintscatalog.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import com.jts.complaintscatalog.R;
import com.jts.complaintscatalog.utils.manager.FontManager;

//Custom text view with different fonts
public class FontTextView extends TextView {

    public FontTextView(Context context) {
        super(context);
    }

    public FontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public FontTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    //Set custom typeface
    private void init(Context context, AttributeSet attrs) {
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.FontTextView);

        if (attributes != null) {
            String fontAsset = attributes.getString(R.styleable.FontTextView_typefaceAsset);

            Typeface font = FontManager.getFont(fontAsset);

            int style = Typeface.NORMAL;

            if (getTypeface() != null) {
                style = getTypeface().getStyle();
            }

            if (font != null) {
                setTypeface(font, style);
            } else {
                Log.d("FontText", String.format("Could not create a font from asset: %s", fontAsset));
            }
        }
        attributes.recycle();
    }
}
