package com.jts.complaintscatalog.utils.converter;

import java.util.Date;

/**
 * Counts days left to deadline for resolving Ticket
 */
public class DaysToDeadlineCounter {
    public static String count(long dateCreatedUnix, long deadlineUnix) {
        Date dateCreated = convertTimestampToDate(dateCreatedUnix);
        Date deadline = convertTimestampToDate(deadlineUnix);

        long daysLeft = (long) Math.round(((deadline.getTime() - dateCreated.getTime())
                / (1000 * 60 * 60 * 24)));

        if (daysLeft < 0) {
            daysLeft = 0;
        }
        return String.valueOf(daysLeft);
    }

    private static Date convertTimestampToDate(long dateUnix) {
        Date date = new Date();
        date.setTime(dateUnix * 1000);
        return date;
    }
}
