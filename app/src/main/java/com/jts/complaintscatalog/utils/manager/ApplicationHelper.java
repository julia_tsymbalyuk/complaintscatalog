package com.jts.complaintscatalog.utils.manager;

import android.app.Application;
import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;

//Provides access to global static controllers, services and resources
public class ApplicationHelper extends Application {
    private static Context mContext;
    private static NetworkService mNetworkService;
    private static RealmController mRealmController;

    public static Context getContext() {
        return mContext;
    }

    public static NetworkService getNetworkService() {
        return mNetworkService;
    }

    public static RealmController getRealmController() {
        return mRealmController;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        mNetworkService = new NetworkService();

        // Configure Realm for the application
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this).build();
        Realm.deleteRealm(realmConfiguration); // Clean slate
        Realm.setDefaultConfiguration(realmConfiguration); // Make this Realm the default
        mRealmController = RealmController.with(this);
    }
}
