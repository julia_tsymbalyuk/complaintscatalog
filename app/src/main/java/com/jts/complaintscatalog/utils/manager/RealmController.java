package com.jts.complaintscatalog.utils.manager;


import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.facebook.AccessToken;
import com.jts.complaintscatalog.data.ticket.Ticket;
import com.jts.complaintscatalog.data.ticket.TicketTab;
import com.jts.complaintscatalog.data.user.User;

import java.util.Collections;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

//Manages work with Realm database
public class RealmController {
    private static RealmController sInstance;
    private Realm mRealm;

    public RealmController(Application application) {
        mRealm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {
        if (sInstance == null) {
            sInstance = new RealmController(fragment.getActivity().getApplication());
        }
        return sInstance;
    }

    public static RealmController with(Activity activity) {
        if (sInstance == null) {
            sInstance = new RealmController(activity.getApplication());
        }
        return sInstance;
    }

    public static RealmController with(Application application) {
        if (sInstance == null) {
            sInstance = new RealmController(application);
        }
        return sInstance;
    }

    public static RealmController getInstance() {
        return sInstance;
    }

    public Realm getRealm() {
        return mRealm;
    }

    //find all objects in the User.class
    public RealmResults<User> getUsers() {
        return mRealm.where(User.class).findAll();
    }

    public boolean hasUsers() {
        return !getUsers().isEmpty();
    }

    public void saveUser(User user) {
        mRealm.beginTransaction();
        mRealm.copyToRealmOrUpdate(user);
        mRealm.commitTransaction();
    }

    public User getUser(String userId) {
        mRealm.beginTransaction();
        User user = mRealm.where(User.class)
                .equalTo("mId", userId)
                .findFirst();
        mRealm.commitTransaction();
        return user;
    }

    //@return User is accessToken is valid or @return null if user is loged out
    public User getUserIfLoggedIn(AccessToken accessToken) {
        User user = null;
        if (hasUsers()) {
            mRealm.beginTransaction();
            user = mRealm.where(User.class)
                    .equalTo("mFacebookAccessToken", accessToken.getToken())
                    .findFirst();
            mRealm.commitTransaction();
        }
        return user;
    }

    public void deleteUser(User user) {
        mRealm.beginTransaction();
        mRealm.where(User.class)
                .equalTo("mId", user.getId())
                .findFirst()
                .deleteFromRealm();
        mRealm.commitTransaction();
    }

    public void deleteTickets(TicketTab ticketStatus) {
        mRealm.beginTransaction();
        getTickets(ticketStatus).deleteAllFromRealm();
        mRealm.commitTransaction();
    }

    //clear all objects from Ticket.class
    public void deleteAllTickets() {
        mRealm.beginTransaction();
        RealmResults results = getTickets();
        results.deleteAllFromRealm();
        Log.d("DEBUG", "Realm Cleared");
        mRealm.commitTransaction();
    }

    //find all objects in the Ticket.class
    public RealmResults<Ticket> getTickets() {
        return mRealm.where(Ticket.class).findAll();
    }

    //query a single item with the given id
    public Ticket getTicket(String id) {
        return mRealm.where(Ticket.class).equalTo("mId", id).findFirst();
    }

    //check if Ticket.class is empty
    public boolean hasTickets() {
        return !getTickets().isEmpty();
    }

    public RealmResults<Ticket> saveTickets(List<Ticket> tickets) {
        mRealm.beginTransaction();
        mRealm.copyToRealmOrUpdate(tickets);
        mRealm.commitTransaction();
        RealmResults<Ticket> results = getTickets();
        return results;
    }

    public RealmResults<Ticket> getTickets(TicketTab ticketStatus) {
        switch (ticketStatus) {
            case IN_PROGRESS:
                return mRealm.where(Ticket.class)
                        .equalTo("mTicketState.mId", 0)
                        .or()
                        .equalTo("mTicketState.mId", 9)
                        .or()
                        .equalTo("mTicketState.mId", 5)
                        .or()
                        .equalTo("mTicketState.mId", 7)
                        .or()
                        .equalTo("mTicketState.mId", 8)
                        .findAll()
                        .sort("mCreatedDate", Sort.DESCENDING);
            case DONE:
                return mRealm.where(Ticket.class)
                        .equalTo("mTicketState.mId", 10)
                        .or()
                        .equalTo("mTicketState.mId", 6)
                        .findAll()
                        .sort("mCreatedDate", Sort.DESCENDING);
            case PENDING:
                return mRealm.where(Ticket.class)
                        .equalTo("mTicketState.mId", 1)
                        .or()
                        .equalTo("mTicketState.mId", 3)
                        .or()
                        .equalTo("mTicketState.mId", 4)
                        .findAll()
                        .sort("mCreatedDate", Sort.DESCENDING);
            default:
                Log.d("DEBUG", "Realm No matches");
                return (RealmResults) Collections.emptyList();
        }
    }

    public void closeRealm() {
        if (!mRealm.isClosed()) {
            Log.d("DEBUG", "Realm closing");
            mRealm.close();
            mRealm = null;
            sInstance = null;
        }
    }
}