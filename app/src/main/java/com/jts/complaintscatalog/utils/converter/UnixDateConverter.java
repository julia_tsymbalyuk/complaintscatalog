package com.jts.complaintscatalog.utils.converter;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Converts unix timestamp into formatted date
 */
public class UnixDateConverter {
    public static String convert(long timestamp) {
        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(timestamp * 1000);
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        return sdf.format(date.getTime());
    }
}