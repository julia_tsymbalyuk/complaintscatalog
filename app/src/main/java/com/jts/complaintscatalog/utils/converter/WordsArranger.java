package com.jts.complaintscatalog.utils.converter;

import android.content.res.Resources;

import com.jts.complaintscatalog.R;
import com.jts.complaintscatalog.utils.manager.ApplicationHelper;

//Utilitary class for working with words
public class WordsArranger {
    //Concats initial string with "day" word depending on a system language
    public static String addDaysWord(String str) {
        Resources resources = ApplicationHelper.getContext().getResources();
        String daysWord = resources.getString(R.string.days_word);
        StringBuilder stringBuilder = new StringBuilder(str);
        return stringBuilder.append(" ").append(daysWord).toString();
    }
}
