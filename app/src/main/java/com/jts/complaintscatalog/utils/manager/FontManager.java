/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the My Own Virtual License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.fakelicenses.com/license
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jts.complaintscatalog.utils.manager;

import android.content.res.AssetManager;
import android.graphics.Typeface;

import java.util.HashMap;
import java.util.Map;

//Holds map with custom Typefaces from fonts stored in Assets.
public class FontManager {
    private static AssetManager sAssetManager;
    private static Map<String, Typeface> sFontsMap;

    private FontManager(AssetManager assetManager) {
        sAssetManager = assetManager;
        sFontsMap = new HashMap<>();
    }

    public static FontManager newInstance(AssetManager assetManager) {
        FontManager fontManager = new FontManager(assetManager);
        return fontManager;
    }

    public static Typeface getFont(String asset) {
        if (sFontsMap.containsKey(asset)) {
            return sFontsMap.get(asset);
        }

        Typeface font = Typeface.createFromAsset(sAssetManager, asset);
        sFontsMap.put(asset, font);
        return font;
    }
}