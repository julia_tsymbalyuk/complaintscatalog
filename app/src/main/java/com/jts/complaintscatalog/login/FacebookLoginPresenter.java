package com.jts.complaintscatalog.login;

public class FacebookLoginPresenter implements FacebookLoginContract.UserActionsListener{
    private FacebookLoginContract.View mView;

    public FacebookLoginPresenter(FacebookLoginContract.View mView) {
        this.mView = mView;
    }

    @Override
    public void makeFacebookLogin() {
        mView.showProfile();
    }
}
