package com.jts.complaintscatalog.login;

/**
 * This specifies the contract between the view and the presenter.
 */
public interface FacebookLoginContract {
    interface View{
        void showProfile();
    }

    interface UserActionsListener{
        void makeFacebookLogin();
    }
}
