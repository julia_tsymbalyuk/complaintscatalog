/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the My Own Virtual License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.fakelicenses.com/license
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jts.complaintscatalog.login;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jts.complaintscatalog.R;
import com.jts.complaintscatalog.data.ticket.TicketTab;
import com.jts.complaintscatalog.tickets.TicketsFragment;
import com.jts.complaintscatalog.utils.FontTextView;
import com.jts.complaintscatalog.utils.manager.FacebookManager;
import com.jts.complaintscatalog.utils.manager.FontManager;
import com.jts.complaintscatalog.utils.manager.RealmController;

import java.util.ArrayList;
import java.util.List;

//Shows lists with Tickets overviews on different tabs, named by Ticket statuses.
//Has navigation drawer with menu.
public class MainActivity extends AppCompatActivity implements FacebookLoginContract.View {
    private FragmentManager mFragmentManager;
    private DrawerManager mDrawerManager;
    private FacebookManager mFacebookManager;
    private FacebookLoginContract.UserActionsListener mLoginPresenter;
    private RealmController mRealmController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Initialize FontManager before setting content view.
        //Because FontManager retrieves fonts from assets.
        //And these fonts will be set to custom text views in main layout
        FontManager.newInstance(getAssets());

        setContentView(R.layout.main_layout_with_drawer);

        mFragmentManager = getSupportFragmentManager();
        mFacebookManager = new FacebookManager(this);

        setUI();
        mLoginPresenter = new FacebookLoginPresenter(this);
        mRealmController = RealmController.with(this);
    }

    private void setUI() {
        setToolbar();
        setTabLayout();
        mDrawerManager = new DrawerManager();
        setFacebookLoginListener();
    }

    private void setToolbar() {
        //Set up toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setNavigationIcon(R.drawable.ic_menu);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        //Set status bar color for android versions > 5
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window w = getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setStatusBarColor(ContextCompat.getColor(this, R.color.primaryDark));
        }
    }

    private void setTabLayout() {
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText(R.string.ticket_ui_status_in_progress));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.ticket_ui_status_done));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.ticket_ui_status_pending));

        //Set tab layout with tab adapter into ViewPager
        final ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        TabAdapter tabAdapter = new TabAdapter(mFragmentManager, tabLayout.getTabCount());

        viewPager.setAdapter(tabAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerManager.mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        if (item.getItemId() == R.id.action_filter) {
            //TODO: implement filter
            Toast.makeText(this, R.string.action_filter, Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    public void setFacebookLoginListener() {
        FontTextView fbLogin = (FontTextView) findViewById(R.id.fb_login);
        fbLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoginPresenter.makeFacebookLogin();
            }
        });
    }

    @Override
    public void showProfile() {
        mFacebookManager.makeLogin();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mFacebookManager.notifyActivityLoginResult(requestCode, resultCode, data);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mFragmentManager = null;
        mDrawerManager = null;
//        mFacebookManager.makeLogout();
        mFacebookManager = null;

        try {
            mRealmController.closeRealm();
        } catch (NullPointerException npe) {
            Log.d("DEBUG", "Trying to close already closed or not initialized Realm db " + npe.getCause().getMessage());
        }
    }

    //Determines what fragment to show depending on selected tab
    private class TabAdapter extends FragmentStatePagerAdapter {
        private int mNumberOfTabs;
        private List<Fragment> mFragmentList;

        public TabAdapter(FragmentManager fm, int numberOfTabs) {
            super(fm);
            mNumberOfTabs = numberOfTabs;
            mFragmentList = new ArrayList<>(mNumberOfTabs);
        }

        @Override
        public Fragment getItem(int position) {
            try {
                mFragmentList.get(position);
            } catch (IndexOutOfBoundsException | NullPointerException ex) {
                switch (position) {
                    case 0:
                        mFragmentList.add(position, TicketsFragment.newInstance(TicketTab.IN_PROGRESS));
                        break;
                    case 1:
                        mFragmentList.add(position, TicketsFragment.newInstance(TicketTab.DONE));
                        break;
                    case 2:
                        mFragmentList.add(position, TicketsFragment.newInstance(TicketTab.PENDING));
                        break;
                }
            }
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mNumberOfTabs;
        }
    }

    //Creates navigation drawer
    private class DrawerManager {
        private DrawerLayout mDrawerLayout;
        private ListView mDrawerList;
        private ActionBarDrawerToggle mDrawerToggle;

        //Creates drawer with menu
        public DrawerManager() {
            setLinksToText(MainActivity.this);
            createDrawerMenu(MainActivity.this);
        }

        private void setLinksToText(AppCompatActivity parent) {
            //Set links to copyright text view
            mDrawerLayout = (DrawerLayout) parent.findViewById(R.id.main_layout_with_drawer);
            TextView drawerCopyright = (TextView) mDrawerLayout.findViewById(R.id.drawer_copyright);
            drawerCopyright.setText(Html.fromHtml(parent.getString(R.string.drawer_copyright)));
            drawerCopyright.setMovementMethod(LinkMovementMethod.getInstance());
        }

        private ActionBarDrawerToggle createDrawerMenu(AppCompatActivity parent) {
            final AppCompatActivity parentActivity = parent;

            List<DrawerMenuItem> drawerMenuItemList = new ArrayList<>();
            drawerMenuItemList.add(new DrawerMenuItem(R.drawable.ic_issues, parentActivity.getString(R.string.drawer_menu_item_all_tickets)));
            drawerMenuItemList.add(new DrawerMenuItem(R.drawable.ic_map, parentActivity.getString(R.string.drawer_menu_item_tickets_on_map)));

            //Set listener to drawer menu items
            mDrawerList = (ListView) parentActivity.findViewById(R.id.drawer_list);
            mDrawerList.setAdapter(new DrawerMenuItemAdapter(parentActivity, drawerMenuItemList));
            mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

            mDrawerToggle = new ActionBarDrawerToggle(
                    parentActivity,        /* host Activity */
                    mDrawerLayout,         /* DrawerLayout object */
                    R.string.drawer_open,  /* "open drawer" description */
                    R.string.drawer_closed /* "close drawer" description */
            ) {
            };

            // Set the drawer toggle as the DrawerListener
            mDrawerLayout.addDrawerListener(mDrawerToggle);

            return mDrawerToggle;
        }


        //Handles clicks on drawer menu items
        private class DrawerItemClickListener implements ListView.OnItemClickListener {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        mDrawerList.setItemChecked(position, true);
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                        break;
                    case 1:
                        //TODO: implement tickets on a map
                        Toast.makeText(view.getContext(), "Tickets on a map", Toast.LENGTH_SHORT).show();
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                        break;
                }
            }
        }


        //Represents menu item in Navigation Drawer
        private class DrawerMenuItem {
            private int mImgResource;
            private String mName;

            //@param imgResource - icon from Resources
            //@param name - text should be displayed as menu option
            public DrawerMenuItem(int imgResource, String name) {
                this.mImgResource = imgResource;
                this.mName = name;
            }

            public int getImgResource() {
                return mImgResource;
            }

            public String getName() {
                return mName;
            }
        }

        //Fills Navigation Drawer menu with data
        private class DrawerMenuItemAdapter extends ArrayAdapter<DrawerMenuItem> {

            public DrawerMenuItemAdapter(Context context, List<DrawerMenuItem> items) {
                super(context, 0, items);
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                DrawerMenuItem item = getItem(position);
                if (convertView == null) {
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.drawer_menu_item, parent, false);
                }
                ImageView itemIcon = (ImageView) convertView.findViewById(R.id.drawer_menu_list_item_icon);
                TextView itemTextView = (TextView) convertView.findViewById(R.id.drawer_menu_list_item);

                itemIcon.setImageResource(item.getImgResource());
                itemTextView.setText(item.getName());

                return convertView;
            }
        }

    }
}
