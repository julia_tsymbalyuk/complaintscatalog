package com.jts.complaintscatalog.data.ticket;

import android.databinding.Bindable;
import android.databinding.Observable;
import android.databinding.PropertyChangeRegistry;

import com.google.gson.annotations.SerializedName;
import com.jts.complaintscatalog.BR;
import com.jts.complaintscatalog.data.user.User;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

public class Ticket extends RealmObject implements Observable {

    @Ignore
    private PropertyChangeRegistry mCallbacks;

    @PrimaryKey
    @SerializedName("id")
    private String mId;
    @SerializedName("user")
    private User mUser;
    @SerializedName("category")
    private TicketCategory mTicketCategory;
    @SerializedName("type")
    private TicketType mTicketType;
    @SerializedName("title")
    private String mTitle;
    @SerializedName("body")
    private String mBody;
    @SerializedName("created_date")
    private long mCreatedDate;
    @SerializedName("start_date")
    private long mStartDate;
    @SerializedName("completed_date")
    private long mCompletedDate;
    @SerializedName("ticket_id")
    private String mTicketId;
    @SerializedName("state")
    private TicketState mTicketState;
    @SerializedName("files")
    private RealmList<TicketAttach> mFiles;
    @SerializedName("performers")
    private RealmList<Performer> mPerformers;
    @SerializedName("deadline")
    private long mDeadline;
    @SerializedName("likes_counter")
    private String mLikesCounter;

    public Ticket() {
        super();
    }

    @Bindable
    public String getId() {
        return mId;
    }

    @Bindable
    public User getUser() {
        return mUser;
    }

    @Bindable
    public TicketCategory getTicketCategory() {
        return mTicketCategory;
    }

    public void setTicketCategory(TicketCategory ticketCategory) {
        mTicketCategory = ticketCategory;
        notifyPropertyChanged(BR.ticketCategory);
    }

    @Bindable
    public TicketType getTicketType() {
        return mTicketType;
    }

    public void setTicketType(TicketType ticketType) {
        mTicketType = ticketType;
        notifyPropertyChanged(BR.ticketType);
    }

    @Bindable
    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
        notifyPropertyChanged(BR.title);
    }

    @Bindable
    public String getBody() {
        return mBody;
    }

    public void setBody(String body) {
        mBody = body;
        notifyPropertyChanged(BR.body);
    }

    @Bindable
    public long getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(long createdDate) {
        mCreatedDate = createdDate;
        notifyPropertyChanged(BR.createdDate);
    }

    @Bindable
    public long getStartDate() {
        return mStartDate;
    }

    public void setStartDate(long startDate) {
        mStartDate = startDate;
        notifyPropertyChanged(BR.startDate);
    }

    @Bindable
    public long getCompletedDate() {
        return mCompletedDate;
    }

    public void setCompletedDate(long completedDate) {
        mCompletedDate = completedDate;
        notifyPropertyChanged(BR.completedDate);
    }

    @Bindable
    public TicketState getTicketState() {
        return mTicketState;
    }

    public void setTicketState(TicketState ticketState) {
        mTicketState = ticketState;
        notifyPropertyChanged(BR.ticketState);
    }

    @Bindable
    public String getTicketId() {
        return mTicketId;
    }

    public void setTicketId(String ticketId) {
        mTicketId = ticketId;
        notifyPropertyChanged(BR.ticketId);
    }

    @Bindable
    public RealmList<TicketAttach> getFiles() {
        return mFiles;
    }

    public void setFiles(RealmList<TicketAttach> files) {
        mFiles = files;
        notifyPropertyChanged(BR.files);
    }

    @Bindable
    public RealmList getPerformers() {
        return mPerformers;
    }

    public void setPerformers(RealmList<Performer> performers) {
        mPerformers = performers;
        notifyPropertyChanged(BR.performers);
    }

    @Bindable
    public long getDeadline() {
        return mDeadline;
    }

    public void setDeadline(long deadline) {
        mDeadline = deadline;
        notifyPropertyChanged(BR.deadline);
    }

    @Bindable
    public String getLikesCounter() {
        return mLikesCounter;
    }

    public void setLikesCounter(String likesCounter) {
        mLikesCounter = likesCounter;
        notifyPropertyChanged(BR.likesCounter);
    }

    public void setmUser(User user) {
        mUser = user;
        notifyPropertyChanged(BR.user);
    }

    public void setmTicketCategory(TicketCategory mTicketCategory) {
        this.mTicketCategory = mTicketCategory;
        notifyPropertyChanged(BR.ticketCategory);
    }

    public void setmTicketType(TicketType mTicketType) {
        this.mTicketType = mTicketType;
        notifyPropertyChanged(BR.ticketType);
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
        notifyPropertyChanged(BR.title);
    }

    public void setmBody(String mBody) {
        this.mBody = mBody;
        notifyPropertyChanged(BR.body);
    }

    public void setmCreatedDate(long mCreatedDate) {
        this.mCreatedDate = mCreatedDate;
        notifyPropertyChanged(BR.createdDate);
    }

    public void setmStartDate(long mStartDate) {
        this.mStartDate = mStartDate;
        notifyPropertyChanged(BR.startDate);
    }

    public void setmCompletedDate(long mCompletedDate) {
        this.mCompletedDate = mCompletedDate;
        notifyPropertyChanged(BR.completedDate);
    }

    public void setmTicketId(String mTicketId) {
        this.mTicketId = mTicketId;
        notifyPropertyChanged(BR.ticketId);
    }

    public void setmTicketState(TicketState mTicketState) {
        this.mTicketState = mTicketState;
        notifyPropertyChanged(BR.ticketState);
    }

    public void setmFiles(RealmList<TicketAttach> mFiles) {
        this.mFiles = mFiles;
        notifyPropertyChanged(BR.files);
    }

    public void setmDeadline(long mDeadline) {
        this.mDeadline = mDeadline;
        notifyPropertyChanged(BR.deadline);
    }

    public void setmLikesCounter(String mLikesCounter) {
        this.mLikesCounter = mLikesCounter;
        notifyPropertyChanged(BR.likesCounter);
    }

    @Override
    public synchronized void addOnPropertyChangedCallback(OnPropertyChangedCallback onPropertyChangedCallback) {
        if (mCallbacks == null) {
            mCallbacks = new PropertyChangeRegistry();
        }
        mCallbacks.add(onPropertyChangedCallback);
    }

    @Override
    public synchronized void removeOnPropertyChangedCallback(OnPropertyChangedCallback onPropertyChangedCallback) {
        if (mCallbacks != null) {
            mCallbacks.remove(onPropertyChangedCallback);
        }
    }

    public synchronized void notifyChange() {
        if (mCallbacks != null) {
            mCallbacks.notifyCallbacks(this, 0, null);
        }
    }

    public void notifyPropertyChanged(int fieldId) {
        if (mCallbacks != null) {
            mCallbacks.notifyCallbacks(this, fieldId, null);
        }
    }
}