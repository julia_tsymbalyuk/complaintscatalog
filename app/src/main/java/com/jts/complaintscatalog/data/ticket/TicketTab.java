package com.jts.complaintscatalog.data.ticket;


public enum TicketTab {
    IN_PROGRESS("0,9,5,7,8"),
    DONE("10,6"),
    PENDING("1,3,4");

    private String ticketStates;


    TicketTab(final String ticketStates) {
        this.ticketStates = ticketStates;
    }

    public String getTicketStates() {
        return ticketStates;
    }

    @Override
    public String toString() {
        return this.name() + "; " + this.getTicketStates();
    }
}
