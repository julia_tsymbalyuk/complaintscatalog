package com.jts.complaintscatalog.data.user;

import android.databinding.Bindable;
import android.databinding.Observable;
import android.databinding.PropertyChangeRegistry;

import com.google.gson.annotations.SerializedName;
import com.jts.complaintscatalog.BR;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

public class User extends RealmObject implements Observable{
    @PrimaryKey
    @SerializedName("id")
    private String mId;
    @SerializedName("first_name")
    private String mFirstName;
    @SerializedName("last_name")
    private String mLastName;
    @SerializedName("middle_name")
    private String mMiddleName;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("birthday")
    private String mBirthday;
    @SerializedName("image")
    private String mImage;
    @SerializedName("phone")
    private String mPhone;
//    @SerializedName("address")
//    private Address mAdress;
    @SerializedName("push_token")
    private String mPushToken;
    @SerializedName("device_type")
    private int mDeviceType;
    @SerializedName("fb_registered")
    private int mFbRegistered;
    @SerializedName("access_token")
    private String mFacebookAccessToken;

    @Ignore
    private PropertyChangeRegistry mCallbacks;

    @Bindable
    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    @Bindable
    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    @Bindable
    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    @Bindable
    public String getMiddleName() {
        return mMiddleName;
    }

    public void setMiddleName(String middleName) {
        mMiddleName = middleName;
    }

    @Bindable
    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    @Bindable
    public String getBirthday() {
        return mBirthday;
    }

    public void setBirthday(String birthday) {
        mBirthday = birthday;
    }

    @Bindable
    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    @Bindable
    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

//    @Bindable
//    public Address getAdress() {
//        return mAdress;
//    }
//
//    public void setmAdress(Address mAdress) {
//        this.mAdress = mAdress;
//    }

    @Bindable
    public String getPushToken() {
        return mPushToken;
    }

    public void setPushToken(String pushToken) {
        mPushToken = pushToken;
    }

    @Bindable
    public int getDeviceType() {
        return mDeviceType;
    }

    public void setDeviceType(int deviceType) {
        mDeviceType = deviceType;
    }

    @Bindable
    public int getFbRegistered() {
        return mFbRegistered;
    }

    public void setFbRegistered(int fbRegistered) {
        this.mFbRegistered = fbRegistered;
    }

    @Bindable
    public String getFacebookAccessToken() {
        return mFacebookAccessToken;
    }

    public void setFacebookAccessToken(String facebookAccessToken) {
        mFacebookAccessToken = facebookAccessToken;
        notifyPropertyChanged(BR.facebookAccessToken);
    }

//    public class Address {
//        @SerializedName("id")
//        private long mId;
//        @SerializedName("district")
//        private District mDistrict;
//        @SerializedName("city")
//        private City mCity;
//        @SerializedName("street")
//        private Street mStreet;
//        @SerializedName("house")
//        private House mHouse;
//        @SerializedName("flat")
//        private int mFlat;
//
//        public class District {
//            @SerializedName("id")
//            private int mId;
//            @SerializedName("name")
//            private String mName;
//            @SerializedName("ru_name")
//            private String mRussianName;
//        }
//
//        public class City {
//            @SerializedName("id")
//            private int mId;
//            @SerializedName("name")
//            private String mName;
//            @SerializedName("ru_name")
//            private String mRussianName;
//        }
//
//        public class Street {
//            @SerializedName("id")
//            private int mId;
//            @SerializedName("name")
//            private String mName;
//            @SerializedName("ru_name")
//            private String mRussianName;
//            @SerializedName("street_type")
//            private StreetType mStreetType;
//
//            public class StreetType {
//                @SerializedName("id")
//                private long mId;
//                @SerializedName("name")
//                private String mName;
//                @SerializedName("short_name")
//                private String mShortName;
//            }
//        }
//
//        public class House {
//            @SerializedName("id")
//            private long mId;
//            @SerializedName("name")
//            private String mName;
//        }
//    }

    @Override
    public synchronized void addOnPropertyChangedCallback(OnPropertyChangedCallback onPropertyChangedCallback) {
        if (mCallbacks == null) {
            mCallbacks = new PropertyChangeRegistry();
        }
        mCallbacks.add(onPropertyChangedCallback);
    }

    @Override
    public synchronized void removeOnPropertyChangedCallback(OnPropertyChangedCallback onPropertyChangedCallback) {
        if (mCallbacks != null) {
            mCallbacks.remove(onPropertyChangedCallback);
        }
    }

    public synchronized void notifyChange() {
        if (mCallbacks != null) {
            mCallbacks.notifyCallbacks(this, 0, null);
        }
    }

    public void notifyPropertyChanged(int fieldId) {
        if (mCallbacks != null) {
            mCallbacks.notifyCallbacks(this, fieldId, null);
        }
    }
}