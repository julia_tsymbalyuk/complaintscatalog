package com.jts.complaintscatalog.data.ticket;

import android.databinding.Bindable;
import android.databinding.Observable;
import android.databinding.PropertyChangeRegistry;

import com.google.gson.annotations.SerializedName;
import com.jts.complaintscatalog.BR;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

public class Performer extends RealmObject implements Observable {
    @Ignore
    private PropertyChangeRegistry mCallbacks;

    @PrimaryKey
    @SerializedName("id")
    private long mId;
    @SerializedName("organization")
    private String mOrganization;
    @SerializedName("person")
    private String mPerson;

    @Bindable
    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
        notifyPropertyChanged(BR.id);
    }

    @Bindable
    public String getOrganization() {
        return mOrganization;
    }

    public void setOrganization(String organization) {
        mOrganization = organization;
        notifyPropertyChanged(BR.organization);
    }

    @Bindable
    public String getPerson() {
        return mPerson;
    }

    public void setPerson(String person) {
        mPerson = person;
        notifyPropertyChanged(BR.person);
    }

    @Override
    public synchronized void addOnPropertyChangedCallback(OnPropertyChangedCallback onPropertyChangedCallback) {
        if (mCallbacks == null) {
            mCallbacks = new PropertyChangeRegistry();
        }
        mCallbacks.add(onPropertyChangedCallback);
    }

    @Override
    public synchronized void removeOnPropertyChangedCallback(OnPropertyChangedCallback onPropertyChangedCallback) {
        if (mCallbacks != null) {
            mCallbacks.remove(onPropertyChangedCallback);
        }
    }

    public synchronized void notifyChange() {
        if (mCallbacks != null) {
            mCallbacks.notifyCallbacks(this, 0, null);
        }
    }

    public void notifyPropertyChanged(int fieldId) {
        if (mCallbacks != null) {
            mCallbacks.notifyCallbacks(this, fieldId, null);
        }
    }
}
